<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>I am Jake Rhenier</title>
        <link rel = "stylesheet" href = "style.css" />
        <link rel = "icon" href = "favicon.png" />
    </head>

    <body>
        <?php
        
        echo '
            <div id = "navContain">
                <nav>
                    <ul>
                        <li>
                            <a href = "#basic-info">
                                <p id = "info-link">Basic information</p>
                            </a>
                        </li>
                        <li>
                            <a href = "#skills-info">
                                <p id = "skill-link">Skills</p>
                            </a>
                        </li>
                        <li>
                            <a href = "#social-info">
                                <p id = "social-link">Social media</p>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        ';

        echo '
            <div class = "welcomePage">
                <p>Hey there! I am Jake.</p>
            </div> 
        ';

        echo '
            <div class = "boxHolder">
                <div class = "informationBox">
                    <div class = "profile-box"><img src = "profile.jpg" id = "profile" /></div>
                    <div class = "categ-box" id = "basic-info">
                        <h1>Basic information<img src = "cat1.png" /></h1>
                        <div class = "info-contain">
                            <div class = "info-type"><h4>Name</h4></div>
                            <div class = "information"><h4>Jake Rhenier Lendio</h4></div>
                        </div>
                        <div class = "info-contain">
                            <div class = "info-type"><h4>Birthdate</h4></div>
                            <div class = "information"><h4>April 9, 1999</h4></div>
                        </div>
                        <div class = "info-contain">
                            <div class = "info-type"><h4>Home address</h4></div>
                            <div class = "information"><h4>New Alegria, Compostela, Compostela Valley Province</h4></div>
                        </div>
                        <div class = "info-contain">
                            <div class = "info-type"><h4>Current address</h4></div>
                            <div class = "information"><h4>Piapi, Boulevard, Davao City</h4></div>
                        </div>
                        <div class = "info-contain">
                            <div class = "info-type"><h4>Email address</h4></div>
                            <div class = "information"><h4>jakerhenier@gmail.com</h4></div>
                        </div>
                        <div class = "info-contain">
                            <div class = "info-type"><h4>Mobile number</h4></div>
                            <div class = "information"><h4>(+63)919 419 7105</h4></div>
                        </div>
                    </div>

                    <div class = "categ-box" id = "skills-info">
                        <h1>Skills<img src = "cat2.png" /></h1>
                        <div class = "skills-contain">
                            <div class = "skill-box">
                                <img src = "skill1.png" />
                                <h4>Fast learner</h4>
                                <p>To be able to catch up every new information that is necessary to the field is an essential feat when doing projects.</p>
                            </div>
                            <div class = "skill-box">
                                <img src = "skill2.png" />
                                <h4>Language-proficient</h4>
                                <p>English, the most used language in the digital world. To freely shape the language to guide the users throughout.</p>
                            </div>
                            <div class = "skill-box">
                                <img src = "skill3.png" />
                                <h4>Adaptive</h4>
                                <p>Using the latest trends to mold the projects to enthuse the users, make it convenient, and very up-to-date.</p>
                            </div>

                            <div class = "statement-box">
                                <p class = "quote" id = "left">"</p>
                                <p id = "main-quote">As a passionate designer, I do believe that designing is more than just about prettying the aesthetics, and that it also means making the experience of the users as pleasant as possible; that everything must be just a click away from their view. In return, this is a great experience for me as well - combining different design principles to create one absolute masterpiece.</p>
                                <p class = "quote" id = "right">"</p>
                            </div>
                        </div>
                    </div>

                    <div class = "categ-box" id = "social-info">
                        <h1>Social media<img src = "cat3.png" /></h1>
                        <p>To know more about me, feel free to check out my social media accounts!</p>
                        <div class = "logo-box">
                            <div class = "platform">
                                <a href = "https://www.facebook.com/yottabytenOugat?ref=bookmarks" target = "_blank">
                                    <img src = "fb.png" />
                                    <h4>Facebook</h4>
                                </a>
                            </div>
                            <div class = "platform">
                                <a href = "https://twitter.com/audiophilicGreg" target = "_blank">
                                    <img src = "twitter.png" />
                                    <h4>Twitter</h4>
                                </a>
                            </div>
                            <div class = "platform">
                                <a href = "https://www.instagram.com/audiophilicgregory/" target = "_blank">
                                    <img src = "ig.png" />
                                    <h4>Instagram</h4>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ';

        ?>
    </body>
</html>